package scheme;


import com.fasterxml.jackson.databind.ObjectMapper;
import realisation.realisation.Bank;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class JSONParser {
    private ObjectMapper objectMapper;

    public JSONParser() {
        this.objectMapper = new ObjectMapper();
    }

    public List<Bank> getBankList(File jsonFile) {
        List<Bank> banks = new ArrayList<>();
        try {
            banks = Arrays.asList(objectMapper.readValue(jsonFile, Bank[].class));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return banks;
    }
}
