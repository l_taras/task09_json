package scheme;


import realisation.realisation.Bank;

import java.io.File;
import java.util.List;

public class JSONUser {

    public static void main(String[] args) {
        File json = new File("src/main/resources/JSON/banks.json");
        File schema = new File("src/main/resources/JSON/banksScheme.json");
        System.out.println(json.exists());
        JSONParser parser = new JSONParser();

        printList(parser.getBankList(json));
    }

    private static void printList(List<Bank> banks) {
        System.out.println("JSON");
        for (Bank bank : banks) {
            System.out.println(bank);
        }
    }
}
